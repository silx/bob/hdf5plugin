# hdf5plugin

[![pipeline status](https://gitlab.esrf.fr/silx/bob/hdf5plugin/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/hdf5plugin/pipelines)

Build binary packages for [hdf5plugin](https://github.com/silx-kit/hdf5plugin): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/hdf5plugin)