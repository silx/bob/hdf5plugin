include:
  - remote: 'https://gitlab.esrf.fr/silx/bob/main/raw/master/job-templates.yml'


stages:
  - build_source
  - build
  - test
  - deploy


variables:
  PROJECT: hdf5plugin
  REPOSITORY: https://github.com/silx-kit/hdf5plugin
  MANYLINUX_PYTHON_VER: cp310-cp310
  MANYLINUX_PPC64LE_PYTHON_VER: cp310-cp310
  HDF5PLUGIN_OPENMP: "False"
  HDF5PLUGIN_NATIVE: "False"
  HDF5PLUGIN_SSE2: "True"
  HDF5PLUGIN_AVX2: "False"
  HDF5PLUGIN_AVX512: "False"
  HDF5PLUGIN_BMI2: "False"
  HDF5PLUGIN_CPP11: "True"
  HDF5PLUGIN_CPP14: "True"
  MACOSX_DEPLOYMENT_TARGET: "10.13"


source:
  extends: .source_build


manylinux2010_wheels:
  extends: .manylinux2010_wheels
  variables:
    MANYLINUX_PLATFORM: manylinux2010_x86_64
    HDF5PLUGIN_CPP11: "False"
    HDF5PLUGIN_CPP14: "False"

manylinux2014_wheels:
  extends: .manylinux2014_wheels

manylinux2014_ppc64le_wheels:
  extends: .manylinux2014_ppc64le_wheels
  variables:
    HDF5PLUGIN_SSE2: "False"

windows_wheel_python3:
  extends: .windows_wheel_python310

macos_x86_wheel:
  extends: .macos11_intel64_wheel_python310

macos_universal2_wheel:
  extends: .macos11_arm64_wheel_python310
  variables:
    PYTHON_VER: "3.10"

# Test

.test_template: &test_template_defintion
  stage: test
  script:
    # Use silx.org for ppc64le wheels
    - pip install --pre --trusted-host www.silx.org --find-links http://www.silx.org/pub/wheelhouse/ --only-binary numpy,h5py numpy h5py
    - pip install --pre --no-index --find-links artifacts/ --only-binary hdf5plugin hdf5plugin
    - python -m hdf5plugin.test
  needs: ["windows_wheel_python3"]

windows_test_python37:
  <<: *test_template_defintion
  extends: .windows_base_python37

windows_test_python38:
  <<: *test_template_defintion
  extends: .windows_base_python38

windows_test_python39:
  <<: *test_template_defintion
  extends: .windows_base_python39

windows_test_python310:
  <<: *test_template_defintion
  extends: .windows_base_python310


# Legacy tests on macos10 laptop

macos10_test_python37:
  <<: *test_template_defintion
  extends: .macos_base_python37
  needs: ["macos_x86_wheel"]

macos10_test_python38:
  <<: *test_template_defintion
  extends: .macos_base_python38
  needs: ["macos_x86_wheel"]

macos10_test_python39:
  <<: *test_template_defintion
  extends: .macos_base_python39
  needs: ["macos_x86_wheel"]


macos_test_x86_python37:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python37
  needs: ["macos_x86_wheel"]

macos_test_x86_python38:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python38
  needs: ["macos_x86_wheel"]

macos_test_x86_python39:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python39
  needs: ["macos_x86_wheel"]

macos_test_x86_python310:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python310
  needs: ["macos_x86_wheel"]


macos_test_universal2_intel64_python38:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python38
  needs: ["macos_universal2_wheel"]

macos_test_universal2_intel64_python39:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python39
  needs: ["macos_universal2_wheel"]

macos_test_universal2_intel64_python310:
  <<: *test_template_defintion
  extends: .macos11_intel64_base_python310
  needs: ["macos_universal2_wheel"]


macos_test_universal2_arm64_python38:
  <<: *test_template_defintion
  extends: .macos11_arm64_base_python38
  needs: ["macos_universal2_wheel"]

macos_test_universal2_arm64_python39:
  <<: *test_template_defintion
  extends: .macos11_arm64_base_python39
  needs: ["macos_universal2_wheel"]

macos_test_universal2_arm64_python310:
  <<: *test_template_defintion
  extends: .macos11_arm64_base_python310
  needs: ["macos_universal2_wheel"]


manylinux2010_test_cp37:
  <<: *test_template_defintion
  extends: .manylinux2010_cp37_base
  needs: ["manylinux2010_wheels"]

manylinux2010_test_cp38:
  <<: *test_template_defintion
  extends: .manylinux2010_cp38_base
  needs: ["manylinux2010_wheels"]

manylinux2010_test_cp39:
  <<: *test_template_defintion
  extends: .manylinux2010_cp39_base
  needs: ["manylinux2010_wheels"]


manylinux2014_test_cp37:
  <<: *test_template_defintion
  extends: .manylinux2014_cp37_base
  needs: ["manylinux2014_wheels"]

manylinux2014_test_cp38:
  <<: *test_template_defintion
  extends: .manylinux2014_cp38_base
  needs: ["manylinux2014_wheels"]

manylinux2014_test_cp39:
  <<: *test_template_defintion
  extends: .manylinux2014_cp39_base
  needs: ["manylinux2014_wheels"]


manylinux2014_ppc64le_test_cp37:
  <<: *test_template_defintion
  extends: .manylinux2014_ppc64le_cp37_base
  needs: ["manylinux2014_ppc64le_wheels"]

manylinux2014_ppc64le_test_cp38:
  <<: *test_template_defintion
  extends: .manylinux2014_ppc64le_cp38_base
  needs: ["manylinux2014_ppc64le_wheels"]

manylinux2014_ppc64le_test_cp39:
  <<: *test_template_defintion
  extends: .manylinux2014_ppc64le_cp39_base
  needs: ["manylinux2014_ppc64le_wheels"]


# Test install from source

.test_sdist_template: &test_sdist_template_defintion
  stage: test
  needs: ["source"]
  script:
    - pip install --pre --only-binary numpy,h5py numpy h5py
    - pip download py-cpuinfo==9.0.0 setuptools wheel
    - pip install --pre --no-index --find-links artifacts/ --find-links ./ --no-binary hdf5plugin hdf5plugin
    - python -m hdf5plugin.test

manylinux2010_test_sdist_cp38:
  <<: *test_sdist_template_defintion
  extends: .manylinux2010_cp38_base

macos_test_sdist_python38:
  <<: *test_sdist_template_defintion
  extends: .macos_base_python38

windows_test_sdist_python38:
  stage: test
  needs: ["source"]
  script:
    - call "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\BuildTools\\VC\\Auxiliary\\Build\\vcvarsall.bat" amd64 -vcvars_ver=14.16
    - pip install --pre --only-binary numpy,h5py numpy h5py
    - pip download py-cpuinfo==9.0.0 setuptools wheel
    - pip install --pre --no-index --find-links artifacts/ --find-links ./ --no-binary hdf5plugin hdf5plugin
    - python -m hdf5plugin.test
  extends: .windows_base_python38


# Deploy

pages:
  extends: .pages
